﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GraficoTorta.ascx.cs" Inherits="EjemploGrafico.Controles.GraficoTorta" %>
<canvas id="grafico" runat="server"></canvas>
<asp:HiddenField ID="hidDataset" runat="server" />

<script>

    function getRandomColor() {
        return '#' + Math.random().toString(16).slice(2, 8).toUpperCase();
    }

    (function () {

        var ctx = document.getElementById('<%=grafico.ClientID%>');
        var dataset = JSON.parse('<%=hidDataset.Value%>');
        var labels = dataset.Data.map(x => x.Label);
        var values = dataset.Data.map(x => x.Value);
        var colores = dataset.Data.map(x => getRandomColor());

        console.log(labels, values);

        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [
                    {
                        backgroundColor: colores,
                        data: values
                    }
                ],
                labels: labels
            }
        });


    })();


</script>
