﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EjemploGrafico
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            GraficoTorta.AgregarDatavalue("Varones", 40);
            GraficoTorta.AgregarDatavalue("Mujeres", 60);
            GraficoTorta.DataBind();

        }
    }
}